from rest_framework import views, generics, mixins, status, viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.contrib.auth import get_user_model

from tsp.applications.comments.constants import COMMENT_CONTENT_TYPE
from tsp.applications.comments.models import ThreadedComment

from .serializers import (
    ThreadedCommentCreateSerializer, ThreadedCommentReviewSerializer,
    ThreadedCommentRecursiveReviewSerializer, ThreadedCommentUserReviewSerializer
)
from .filters import ThreadedCommentBaseFilter, ThreadedCommentFilter


User = get_user_model()


class TCCreateView(generics.CreateAPIView):
    """ Создание комментария """
    permission_classes = [IsAuthenticated]
    serializer_class = ThreadedCommentCreateSerializer
    serializer_class_review = ThreadedCommentReviewSerializer
    queryset = ThreadedComment.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        instance = serializer.instance
        headers = self.get_success_headers(serializer.data)
        out_serializer = self.serializer_class_review(
            instance=instance,
            context={'request': self.request}
        )
        return Response(
            out_serializer.data, status=status.HTTP_201_CREATED,
            headers=headers,
        )

    def perform_create(self, serializer):
        serializer.save(
            user=self.request.user
        )


class TCEditView(generics.RetrieveUpdateDestroyAPIView):
    """ Изменение, удаление комментария"""
    permission_classes = [IsAuthenticated]
    serializer_class = ThreadedCommentReviewSerializer
    queryset = ThreadedComment.objects.all()

    def get_queryset(self):
        qs = super(TCEditView, self).get_queryset()
        return qs.filter(user=self.request.user)

    def perform_destroy(self, instance):
        if instance.get_children().exists():
            raise ValidationError('У комментария есть потомки')
        instance.delete()


class TCAllRootView(generics.ListAPIView):
    """ Получение комментариев первого уровня для определенной сущности """
    queryset = ThreadedComment.objects.all()
    filter_class = ThreadedCommentFilter
    serializer_class = ThreadedCommentReviewSerializer
    permission_classes = [AllowAny]

    def list(self, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if kwargs.get('content_type') == COMMENT_CONTENT_TYPE:
            try:
                tc = ThreadedComment.objects.get(id=kwargs.get('object_pk'))
                queryset = tc.get_children()
            except:
                queryset = ThreadedComment.objects.none()
        else:
            queryset = queryset.filter_by_ct_opk(**kwargs).filter(parent__isnull=True)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class TCRecursiveView(TCAllRootView):
    """ Получение всех дочерних комментариев для заданного комментария или
        сущности без ограничения по уровню вложенности
    """
    serializer_class = ThreadedCommentRecursiveReviewSerializer


class TCUserView(generics.ListAPIView):
    """ Получение истории комментариев определенного пользователя """
    filter_class = ThreadedCommentBaseFilter
    permission_classes = [AllowAny]
    serializer_class = ThreadedCommentUserReviewSerializer
    queryset = ThreadedComment.objects.all()

    def list(self, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=kwargs.get('pk'))
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

