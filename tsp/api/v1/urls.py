from django.conf.urls import url

from .views import (
    TCCreateView, TCEditView, TCAllRootView, TCRecursiveView, TCUserView
)

urlpatterns = [
    url(r'^tc/$', TCCreateView.as_view(), name='tc_create'),
    url(r'^tc/(?P<pk>[0-9]+)/$', TCEditView.as_view(), name='tc_delete'),
    url(r'^tcall_root/(?P<content_type>[-\w.]+)/(?P<object_pk>[0-9]+)/$',
        TCAllRootView.as_view(), name='tcall_root'
        ),
    url(r'^tc_recursive/(?P<content_type>[-\w.]+)/(?P<object_pk>[0-9]+)/$',
        TCRecursiveView.as_view(), name='tc_recursive'
        ),
    url(r'^tc_user/(?P<pk>[0-9]+)/$',
        TCUserView.as_view(), name='tc_user'
        ),
]

