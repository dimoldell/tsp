from django.contrib.contenttypes.models import ContentType
from rest_framework_recursive.fields import RecursiveField
from rest_framework import serializers

from tsp.applications.comments.constants import CONTENT_TYPE_CHOICE
from tsp.applications.comments.models import ThreadedComment


class ThreadedCommentCreateSerializer(serializers.ModelSerializer):
    """ Serializer Create comment """
    content_type = serializers.ChoiceField(choices=CONTENT_TYPE_CHOICE, required=True)
    object_pk = serializers.IntegerField(required=True)
    comment = serializers.CharField(required=True)

    class Meta:
        model = ThreadedComment
        fields = (
            'content_type', 'object_pk', 'comment'
        )

    def validate_content_type(self, value):
        try:
            content_type = ContentType.objects.get_by_natural_key(*value.split('.'))
        except ContentType.DoesNotExist:
            raise serializers.ValidationError('Сущности не найдено')
        return content_type

    def create(self, validated_data):
        content_type = validated_data.pop('content_type')
        object_pk_value = validated_data.pop('object_pk')
        try:
            content_object = content_type.model_class().objects.get(pk=object_pk_value)
        except content_type.model_class().DoesNotExist:
            raise serializers.ValidationError({'error': 'Объекта комментирования не найдено'})
        validated_data['content_object'] = content_object
        # Задаем родителя комментарию
        if content_type.model_class() is ThreadedComment:
            validated_data['parent'] = content_object
        obj = super(ThreadedCommentCreateSerializer, self).create(validated_data)
        return obj


class ThreadedCommentReviewSerializer(serializers.ModelSerializer):
    """ Serializer Review comments """

    class Meta:
        model = ThreadedComment
        fields = ('id', 'created', 'comment',)


class ThreadedCommentRecursiveReviewSerializer(serializers.ModelSerializer):
    """ Serializer Recursive Review comments """
    children = RecursiveField(required=False, allow_null=True, many=True)

    class Meta:
        model = ThreadedComment
        fields = ('id', 'created', 'comment', 'children')


class ThreadedCommentUserReviewSerializer(serializers.ModelSerializer):
    """ Serializer User comments """
    class Meta:
        model = ThreadedComment
        fields = ('id', 'created', 'comment', 'parent', 'children')
