import django_filters

from tsp.applications.comments.models import ThreadedComment


class ThreadedCommentBaseFilter(django_filters.FilterSet):
    created_from = django_filters.DateTimeFilter(name='created', lookup_expr='gte')
    created_to = django_filters.DateTimeFilter(name='created', lookup_expr='lte')

    class Meta:
        model = ThreadedComment
        fields = ('created_from', 'created_to',)
        order_by = '-created'


class ThreadedCommentFilter(ThreadedCommentBaseFilter):
    user_id = django_filters.NumberFilter(name='user__id')
    user_email = django_filters.CharFilter(name='user__email')

    class Meta(ThreadedCommentBaseFilter.Meta):
        fields = ThreadedCommentBaseFilter.Meta.fields + \
                 ('user_id', 'user_email')
