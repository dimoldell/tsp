from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from .models import ThreadedComment


@admin.register(ThreadedComment)
class ThreadedCommentAdmin(MPTTModelAdmin):
    """ Admin of ThreadedComment """
    fields = (
        'id', 'created', 'user', 'content_type', 'object_pk',
        'parent'
    )
    readonly_fields = ('id', 'created', 'user',)
    raw_id_fields = ('parent',)
