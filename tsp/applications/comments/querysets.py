from django.contrib.contenttypes.models import ContentType
from django.db import models


class TCQuerySet(models.query.QuerySet):

    def filter_by_content_object(self, content_object):
        return self.filter(
            content_type=ContentType.objects.get_for_model(content_object.__class__),
            object_pk=content_object.pk
        )

    def filter_by_ct_opk(self, content_type, object_pk):
        try:
            content_type = ContentType.objects.get_by_natural_key(*content_type.split('.'))
        except:
            return self.none()
        if self.model is content_type.model_class():
            return
        return self.filter(content_type=content_type, object_pk=object_pk)
