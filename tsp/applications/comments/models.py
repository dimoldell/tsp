from django.db import models
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from mptt.models import MPTTModel, TreeForeignKey
from .managers import TCManager

COMMENT_MAX_LENGTH = getattr(settings, 'COMMENT_MAX_LENGTH', 2000)


class ThreadedComment(MPTTModel):
    """
    A Threaded Comment
    """
    objects = TCManager()

    created = models.DateTimeField(verbose_name='Создан', auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Автор')
    content_type = models.ForeignKey(
        ContentType, verbose_name='Тип содержимого', on_delete=models.CASCADE,
        blank=True, null=True
    )
    object_pk = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey(ct_field='content_type', fk_field='object_pk')
    parent = TreeForeignKey(
        'self', null=True, blank=True, related_name='children'
    )
    comment = models.TextField(verbose_name='Комментарий', max_length=COMMENT_MAX_LENGTH)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

    def __str__(self):
        return "ID %s [U %s]" % (
            self.id, self.user_id,
        )

    @classmethod
    def get_all_root(cls, content_object):
        return cls.objects.filter_by_content_object(content_object).\
            filter(parent__isnull=True)
