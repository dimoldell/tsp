from django.shortcuts import render_to_response

from .models import ThreadedComment


def show_comments(request):
    return render_to_response(
        "comment.html", {'comments': ThreadedComment.objects.all()},
    )
