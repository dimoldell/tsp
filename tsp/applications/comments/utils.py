from random import randrange
from .models import ThreadedComment


def gen_node(size, user, node=None):
    size -= 1
    if size <= 0:
        return False
    if not node:
        node = ThreadedComment.objects.create(user=user, comment='MAIN Comment %s' % size)
    node_child = ThreadedComment.objects.create(
        user=user, content_object=node, parent=node, comment='Comment %s' % size
    )
    for i in range(0, randrange(1, 3)):
        gen_node(size, user, node_child)
    return True

