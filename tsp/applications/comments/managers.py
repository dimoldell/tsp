from mptt.managers import TreeManager
from django.db import models
from .querysets import TCQuerySet


class TCManager(models.Manager.from_queryset(TCQuerySet), TreeManager):
    """ """
