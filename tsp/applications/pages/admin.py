from django.contrib import admin

from .models import BlogPost, UserPage


@admin.register(BlogPost)
class BlogPostAdmin(admin.ModelAdmin):
    """ BlogPost Admin """


@admin.register(UserPage)
class UserPageAdmin(admin.ModelAdmin):
    """ UserPage Admin """
