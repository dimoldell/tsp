from django.db import models
from django.conf import settings


class BlogPost(models.Model):
    """
    A Blog Post
    """
    created = models.DateTimeField(verbose_name='Создан', auto_created=True)
    title = models.CharField(verbose_name='Заголовок', max_length=200)
    body_text = models.TextField(verbose_name='Содержимое')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Автор')

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    def __str__(self):
        return "%s %s" % (self.id, self.title)


class UserPage(models.Model):
    """
    A User Page
    """
    created = models.DateTimeField(verbose_name='Создана', auto_created=True)
    body_text = models.TextField(verbose_name='Содержимое')
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, verbose_name='Пользователь',
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'Страница пользователя'
        verbose_name_plural = 'Страницы пользователя'

    def __str__(self):
        return "%s" % self.user_id
