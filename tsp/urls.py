from django.conf.urls import url, include
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view

from tsp.applications.comments.views import show_comments

schema_view = get_swagger_view(title=' API')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^comments/$', show_comments),
    url(r'^api/v1/', include('tsp.api.v1.urls', namespace='v1')),
    url(r'^api-doc/$', schema_view),
]
